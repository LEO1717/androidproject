package ru.yarosh.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    public static final String KEY_ANSWER = "answer";

    private Button answerButton1;
    private Button answerButton2;
    private Button answerButton3;
    private Button answerButton4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        answerButton1 = (Button) findViewById(R.id.btn_answer1);
        answerButton2 = (Button) findViewById(R.id.btn_answer2);
        answerButton3 = (Button) findViewById(R.id.btn_answer3);
        answerButton4 = (Button) findViewById(R.id.btn_answer4);
        initAnsewrButton();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void initAnsewrButton(){
            answerButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String answer = answerButton1.getText().toString();
                    Intent intent = new Intent(MainActivity.this, AnswerActivity.class);
                    intent.putExtra(KEY_ANSWER,answer);
                    startActivity(intent);
                }
            });
        answerButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerButton2.getText().toString();
                Intent intent = new Intent(MainActivity.this, AnswerActivity.class);
                intent.putExtra(KEY_ANSWER,answer);
                startActivity(intent);
            }
        });
        answerButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerButton3.getText().toString();
                Intent intent = new Intent(MainActivity.this, AnswerActivity.class);
                intent.putExtra(KEY_ANSWER,answer);
                startActivity(intent);
            }
        });
        answerButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerButton4.getText().toString();
                Intent intent = new Intent(MainActivity.this, AnswerActivity.class);
                intent.putExtra(KEY_ANSWER,answer);
                startActivity(intent);
            }
        });

    }


}
