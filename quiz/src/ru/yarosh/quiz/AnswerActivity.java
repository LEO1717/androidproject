package ru.yarosh.quiz;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by LEONID on 14.11.2014.
 */
public class AnswerActivity extends Activity {

    TextView txt_answerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answer);

        txt_answerView=(TextView) findViewById(R.id.txt_answer);

        String txt_answer = getIntent().getStringExtra(MainActivity.KEY_ANSWER);

        txt_answerView.setText(getString(R.string.your_answer, txt_answer));

    }
}