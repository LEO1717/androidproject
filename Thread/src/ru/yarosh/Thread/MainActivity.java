package ru.yarosh.Thread;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;

public class MainActivity extends Activity {

    TextView counter;
    Button btn_loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        counter = (TextView) findViewById(R.id.counter);
        btn_loading = (Button) findViewById(R.id.button);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initLoadFilesBtn();
    }

    private void initLoadFilesBtn(){
        btn_loading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thread.start();
            }
        });
    }

    private android.os.Handler handler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            counter.setText(Integer.toString(msg.what));
        }
    };

    private Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<10;i++){
                loading_thread();
                handler.sendEmptyMessage(i+1);
            }
        }
    });

    private void loading_thread(){
        try{
            TimeUnit.SECONDS.sleep(1);
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
