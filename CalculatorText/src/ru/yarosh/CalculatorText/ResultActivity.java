package ru.yarosh.CalculatorText;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by LEONID on 17.11.2014.
 */
public class ResultActivity extends Activity {

    private static final String KEY_INT_NUMBER_ONE = "numberOne";
    private static final String KEY_INT_NUMBER_TWO = "numberTwo";

    private int numberOne;
    private int numberTwo;
    private TextView calculateView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        calculateView = (TextView) findViewById(R.id.txt_result);
        numberOne = getIntent().getIntExtra(MainActivity.KEY_INT_ONE, 0);
        numberTwo = getIntent().getIntExtra(MainActivity.KEY_INT_TWO, 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        resetUI();
    }

    private void resetUI() {
        int idResources = R.string.txt_calculate_positive;
        if (numberTwo < 0) {
            idResources = R.string.txt_calculate_negative;
        }
        calculateView.setText(getString(idResources, numberOne, numberTwo, (numberOne + numberTwo)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_INT_NUMBER_ONE, numberOne);
        outState.putInt(KEY_INT_NUMBER_TWO, numberTwo);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(KEY_INT_NUMBER_ONE)) {
            numberOne = savedInstanceState.getInt(KEY_INT_NUMBER_ONE);
        }
        if (savedInstanceState.containsKey(KEY_INT_NUMBER_TWO)) {
            numberTwo = savedInstanceState.getInt(KEY_INT_NUMBER_TWO);
        }
    }
}