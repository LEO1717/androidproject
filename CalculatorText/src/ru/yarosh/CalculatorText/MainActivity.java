package ru.yarosh.CalculatorText;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
    private static final String KEY_TEXT_ONE = "numberOneEdit";
    private static final String KEY_TEXT_TWO = "numberTwoEdit";
    public static final String KEY_INT_ONE = "numberOne";
    public static final String KEY_INT_TWO = "numberTwo";

    private EditText numberOneEdit;
    private EditText numberTwoEdit;
    private Button amountButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        numberOneEdit = (EditText) findViewById(R.id.edt_summandOne);
        numberTwoEdit = (EditText) findViewById(R.id.edt_summandTwo);
        amountButton = (Button) findViewById(R.id.btn_addition);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initNumberOneEdit();
        initTwoEdit();
        initAmountButton();
    }

    private void initNumberOneEdit(){
        numberOneEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean state) {
                if (!state) {
                    if (numberOneEdit.getText().length() == 0) {
                        numberOneEdit.setError(getResources().getString(R.string.err_empty));
                    }
                }
            }
        });
    }

    private void initTwoEdit(){
        numberTwoEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean state) {
                if (!state) {
                    if(numberTwoEdit.getText().length() == 0) {
                        numberTwoEdit.setError(getResources().getString(R.string.err_empty));
                    }
                }
            }
        });
    }

    private void initAmountButton(){
        amountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCheck()) {
                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                    intent.putExtra(KEY_INT_ONE, Integer.valueOf(numberOneEdit.getText().toString()));
                    intent.putExtra(KEY_INT_TWO, Integer.valueOf(numberTwoEdit.getText().toString()));
                    startActivity(intent);
                }
            }
        });
    }

    private boolean isCheck() {
        if (numberOneEdit.getText().length() == 0) {
            return false;
        }
        if (numberTwoEdit.getText().length() == 0) {
            return false;
        }
        return true;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TEXT_ONE, numberOneEdit.getText().toString());
        outState.putString(KEY_TEXT_TWO, numberTwoEdit.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(KEY_TEXT_ONE)) {
            numberOneEdit.setText(savedInstanceState.getString(KEY_TEXT_ONE, ""));
        }
        if (savedInstanceState.containsKey(KEY_TEXT_TWO)) {
            numberTwoEdit.setText(savedInstanceState.getString(KEY_TEXT_TWO, ""));
        }
    }
}
