package ru.yarosh.helper;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import org.w3c.dom.Text;

/**
 * Created by LEONID on 15.11.2014.
 */
public class ContentActivity extends Activity {

    TextView contentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activit_content);
        contentView = (TextView) findViewById(R.id.txt_content);

        String[] data = getResources().getStringArray(R.array.helper_content);

        Long position = getIntent().getLongExtra(MainActivity.KEY_ID,0);
        contentView.setText(data[position.intValue()]);

    }
}