package ru.yarosh.helper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.security.Key;

public class MainActivity extends Activity {

    public static final  String KEY_ID = "id_title";

    ListView titleList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        titleList = (ListView) findViewById(R.id.list_title);
        String[] data = getResources().getStringArray(R.array.helper_title);
        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        titleList.setAdapter(adapter);

        titleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int id, long position) {
                Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                intent.putExtra(KEY_ID,position);
                startActivity(intent);
            }
        });
    }
}
